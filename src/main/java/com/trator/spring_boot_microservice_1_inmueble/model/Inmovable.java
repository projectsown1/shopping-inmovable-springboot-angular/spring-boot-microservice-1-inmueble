package com.trator.spring_boot_microservice_1_inmueble.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name="inmovables")
public class Inmovable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name="name", length = 150, nullable = false)
    private String name;

    @Column(name="address",length = 500, nullable = false)
    private String address;

    @Column(name = "picture",length = 1200, nullable = true)
    private String picture;

    @Column(name = "price",nullable = false)
    private Double price;

    @Column(name="created_at",nullable = false)
    private LocalDateTime created_at;
}
