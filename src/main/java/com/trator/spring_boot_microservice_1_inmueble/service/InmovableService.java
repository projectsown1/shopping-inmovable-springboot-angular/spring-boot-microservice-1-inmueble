package com.trator.spring_boot_microservice_1_inmueble.service;

import com.trator.spring_boot_microservice_1_inmueble.model.Inmovable;

import java.util.List;

public interface InmovableService {
    Inmovable saveInmovable(Inmovable inmovable);

    void deleteInmovable(Long inmovableId);

    List<Inmovable> findAllInmovables();
}
