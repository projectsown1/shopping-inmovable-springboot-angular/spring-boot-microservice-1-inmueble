package com.trator.spring_boot_microservice_1_inmueble.service;

import com.trator.spring_boot_microservice_1_inmueble.model.Inmovable;
import com.trator.spring_boot_microservice_1_inmueble.repository.InmovableRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class InmovableServiceImpl implements InmovableService{

    private final InmovableRepository inmovableRepository;

    public InmovableServiceImpl(InmovableRepository inmovableRepository) {
        this.inmovableRepository = inmovableRepository;
    }

    @Override
    public Inmovable saveInmovable(Inmovable inmovable){
        inmovable.setCreated_at(LocalDateTime.now());
        return inmovableRepository.save(inmovable);
    }

    @Override
    public void deleteInmovable(Long inmovableId){
        inmovableRepository.deleteById(inmovableId);
    }

    @Override
    public List<Inmovable> findAllInmovables(){
        return inmovableRepository.findAll();
    }
}
