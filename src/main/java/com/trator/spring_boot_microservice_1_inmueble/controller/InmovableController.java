package com.trator.spring_boot_microservice_1_inmueble.controller;

import com.trator.spring_boot_microservice_1_inmueble.model.Inmovable;
import com.trator.spring_boot_microservice_1_inmueble.service.InmovableService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/inmovable")
public class InmovableController {

    private final InmovableService inmovableService;

    public InmovableController(InmovableService inmovableService) {
        this.inmovableService = inmovableService;
    }

    @PostMapping
    public ResponseEntity<?> saveInmovable(@RequestBody Inmovable inmovable){
        return new ResponseEntity<>(
                inmovableService.saveInmovable(inmovable),
                HttpStatus.CREATED
        );
    }

    @DeleteMapping("{inmovableId}")
    public ResponseEntity<?> deleteInmovable(@PathVariable Long inmovableId){
        inmovableService.deleteInmovable(inmovableId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getAllInmovable(){
        return ResponseEntity.ok(inmovableService.findAllInmovables());
    }
}
