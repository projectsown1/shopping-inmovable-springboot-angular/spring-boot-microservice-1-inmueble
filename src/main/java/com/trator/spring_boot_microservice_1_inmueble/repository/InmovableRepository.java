package com.trator.spring_boot_microservice_1_inmueble.repository;

import com.trator.spring_boot_microservice_1_inmueble.model.Inmovable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InmovableRepository extends JpaRepository<Inmovable, Long> {
}
